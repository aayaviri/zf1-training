<?php

class UserController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }
	
	// List of all users
    public function indexAction()
    {
        $page = $this->_getParam('page', 1);

		$userDao = new App_Dao_UserDao();
		$paginator = new App_Util_Paginator($this->getRequest()->getBaseUrl() . '/user/index', $userDao->countAll(), $page);

		$this->view->dataList = $userDao->getAllLimitOffset($paginator->getLimit(), $paginator->getOffset());
		$this->view->htmlPaginator = $paginator->showHtmlPaginator();
    }
	
	// Show details data of an specific user
	public function viewAction()
	{
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');
		
		$userDao = new App_Dao_UserDao();
		$this->view->user = $userDao->getById($id);
	}
	
	// Add a new user
	public function addAction()
	{
		$form = new App_Form_UserForm();

		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();

			if ($form->isValid($formData)) {
				if( $formData['_password'] == $formData['confirmPassword'] ) {
					$user = new App_Model_User();
					$user->setUsername($formData['_username']);
					$user->setPassword(($formData['_password']));
					$user->setRol($formData['_rol']);
					
					$userDao = new App_Dao_UserDao();
					$userDao->save($user);
				
					$this->_helper->redirector('index');
					return;
				}
				else {
					$this->view->message = 'La nueva contrasena no coincide en ambos campos.';
				}
			}
		}
		$this->view->form = $form;
	}
	
	// Modify an existing user
	public function modifyAction()
	{
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');
		
		$userDao = new App_Dao_UserDao();

		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$form = new App_Form_UserForm(true);
			$form->getElement('_password')->setRequired(false);
			$form->getElement('confirmPassword')->setRequired(false);
			
			if ($form->isValid($formData)) {
				$user = $userDao->getById($id);
				
				$user->setUsername($formData['_username']);
				$user->setRol($formData['_rol']);
				
				if($formData['currentPassword'] == ''){
					$userDao->save($user);
					$this->_helper->redirector('index');
					return;
				}
				else{
					if( md5($formData['currentPassword']) === $user->getPassword() ) {
						if( $formData['_password'] == $formData['confirmPassword'] ){
							$user->setPassword($formData['_password']);
							$userDao->save($user);
					
							$this->_helper->redirector('index');
							return;
						}
						else {
							$this->view->message = 'La Nueva contraseña no coincide en ambos campos';
						}
					}
					else {
						$this->view->message = 'Contrasena actual incorrecta.';
					}
				}
			} else
				$form->populate($formData);
		
		} else {
			$id = $this->_getParam('id', '');

			if (empty($id)) {
				$this->_helper->redirector('index');
				return;
			} else
				$form = new App_Form_UserForm(true);
		}

		$user = $userDao->getById($id);
		if (!empty($user))
			$form->populate($user->toArray());

		$this->view->form = $form;
	}
	
	// Remove an existing user
	public function removeAction()
	{
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');
		
		$userDao = new App_Dao_UserDao();

		$user = $userDao->getById($id);
		if(!empty($user))
			$userDao->remove($user);
		
		$this->_helper->redirector('index');
		return;
	}

}

