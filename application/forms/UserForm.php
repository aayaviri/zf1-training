<?php

class App_Form_UserForm extends Zend_Form
{
    public function __construct($value = false)
	{
		parent::__construct();
		
		$this->setMethod('post');
		
		$username = new Zend_Form_Element_Text('_username');
		$username->setLabel("Usuario:");
		$username->setRequired(true);
		$this->addElement($username);
		
		if($value) {
			$currentPassword = new Zend_Form_Element_Password('currentPassword');
			$currentPassword->setLabel('Contrasena Anterior');
			$currentPassword->setRequired(false);
			$this->addElement($currentPassword);
		}
		
		$password = new Zend_Form_Element_Password('_password');
		$password->setLabel("Contrasena:");
		$password->setRequired(true);
		$this->addElement($password);
		
		$confirmPassword = new Zend_Form_Element_Password('confirmPassword');
		$confirmPassword->setLabel('Confirmar contrasena');
		$confirmPassword->setRequired(true);
		$this->addElement($confirmPassword);
		
		$rol = new Zend_Form_Element_Text('_rol');
		$rol->setLabel("Rol:");
		$rol->setRequired(true);
		$this->addElement($rol);
		
		
		// Submit button
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		$this->addElement($submit);
		//$this->addElements(array($username, $password, $rol, $submit));
	}	
}

?>